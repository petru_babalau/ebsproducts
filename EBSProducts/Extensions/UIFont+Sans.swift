import Foundation
import UIKit

enum OpenSans: String {
    case regular = "OpenSans-Regular"
    case bold = "OpenSans-Bold"
    case semiBoldItalic = "OpenSans-SemiBoldItalic"
    case extraBoldItalic = "OpenSans-ExtraBoldItalic"
    case lightItalic = "OpenSans-LightItalic"
    case boldItalic = "OpenSans-BoldItalic"
    case light = "OpenSans-Light"
    case semiBold = "OpenSans-SemiBold"
    case italic = "OpenSans-Italic"
    case extraBold = "OpenSans-ExtraBold"
}

extension UIFont {
    convenience init?(openSans: OpenSans, size: CGFloat) {
        self.init(name: openSans.rawValue, size: size)
    }
}
