import Foundation
import UIKit

extension UINavigationItem {
    func setLogo() {
        titleView = UIImageView(image: #imageLiteral(resourceName: "Logo"))
    }
}
