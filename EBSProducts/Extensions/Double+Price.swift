import Foundation

extension Double {
    var priceFormated: String {
        return String(format: "%g", self)
    }
    
    func discounted(by discount: Double) -> NSAttributedString {
        let price = self - self * discount / 100
        let sellingPrice = "$ \(price.priceFormated),-"
        let defaultPrice = "$ \(priceFormated),-"
        
        let sellingPriceFont = UIFont(openSans: .semiBold, size: 14) ?? UIFont.boldSystemFont(ofSize: 14)
        let sellingPriceAttributedString = NSMutableAttributedString(
            string: sellingPrice,
            attributes: [.font: sellingPriceFont,
                         .foregroundColor: UIColor(r: 43, g: 95, b: 150)])
        sellingPriceAttributedString.append(NSAttributedString(string: " "))
        
        let defaultPriceFont = UIFont(openSans: .semiBold, size: 12) ?? UIFont.boldSystemFont(ofSize: 12)
        let defaultPriceAttributedString = NSAttributedString(
            string: defaultPrice,
            attributes: [.font: defaultPriceFont,
                         .foregroundColor: UIColor(r: 159, g: 175, b: 185),
                         .strikethroughStyle: NSUnderlineStyle.styleSingle.rawValue])
        sellingPriceAttributedString.append(defaultPriceAttributedString)
        
        return sellingPriceAttributedString
    }
}
