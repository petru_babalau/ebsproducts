import Foundation
import UIKit

enum SFProText: String {
    case regular = "SFProText-Regular"
}

extension UIFont {
    convenience init?(SFProText: SFProText, size: CGFloat) {
        self.init(name: SFProText.rawValue, size: size)
    }
}
