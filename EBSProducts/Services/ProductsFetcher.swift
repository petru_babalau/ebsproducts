import Foundation
import POSUtils

struct ProductsFetcher {
    private var baseUrl = URL(string: "http://185.181.231.23:3000")!
    
    func get(offset: Int,
             onSuccess: @escaping ([ProductModel]) -> Void,
             onFailure: @escaping (ErrorType) -> Void) {
        
        let onSuccess: (Any?) -> Void = { response in
            guard let models = response.flatMap([ProductModel].decode) else {
                return onFailure(.responseParse)
            }
            onSuccess(models)
        }
        
        let path = "\(baseUrl.absoluteString)/products?offset=\(offset * 10)&limit=10"
        let inputs = RequestInputs(path: path,
                                   onSuccess: onSuccess,
                                   onFailure: onFailure)
        Request(inputs: inputs, encoding: .data)
    }
    
    func get(by model: ProductModel,
             onSuccess: @escaping (ProductModel) -> Void,
             onFailure: @escaping (ErrorType) -> Void) {
        let onSuccess: (Any?) -> Void = { response in
            guard let result = response.flatMap(ProductModel.decode) else {
                return onFailure(.responseParse)
            }
            onSuccess(result)
        }
        
        let path = "\(baseUrl.absoluteString)/product?id=\(model.id)"
        let inputs = RequestInputs(path: path,
                                   onSuccess: onSuccess,
                                   onFailure: onFailure)
        Request(inputs: inputs, encoding: .data)
    }
}
