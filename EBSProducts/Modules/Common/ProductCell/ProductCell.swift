import Foundation
import UIKit

class ProductCell: UITableViewCell {
    static let identifier = "ProductCell"
    
    @IBOutlet private weak var productImageView: UIImageView!
    @IBOutlet private weak var titleLabel: UILabel!
    @IBOutlet private weak var descriptionlabel: UILabel!
    @IBOutlet private weak var priceLabel: UILabel!
    @IBOutlet private weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet private weak var separator: UIImageView!
    
    func setup(with item: ProductModel) {
        titleLabel.text = item.title
        descriptionlabel.text = item.short_description
        setupImage(item)
        setupPrice(item)
    }
    
    func hideSeparator() {
        separator.isHidden = true
    }
    
    private func setupImage(_ item: ProductModel) {
        guard let url = URL(string: item.image) else { return }
        productImageView.setImageWith(url: url,
                                      placeholderImage: UIImage(color: .black)) { [weak activityIndicator] _ in
                                        activityIndicator?.stopAnimating()
        }
    }
    
    private func setupPrice(_ item: ProductModel) {
        if item.sale_precent == 0 {
            priceLabel.text = "$ \(item.price.priceFormated)"
        } else {
            priceLabel.attributedText = item.price.discounted(by: item.sale_precent)
        }
    }
}
