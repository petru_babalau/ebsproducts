import Foundation

final class ProductsPresenter {

    weak var view: ProductsViewInput!
    var interactor: ProductsInteractorInput!
    var router: ProductsRouterInput!
    
    private var products: [ProductModel] = .zero
    private var offset = 0
    private let loader = LoaderAlert(type: .default)
}

// MARK: - ProductsModuleInput

extension ProductsPresenter: ProductsModuleInput {
	
}

// MARK: - ProductsViewOutput

extension ProductsPresenter: ProductsViewOutput {
	func viewIsReady() {
		router.view = view
        view.setupInitialState()
        requestProducts()
    }
    
    func requestProducts() {
        view.showLoader(loader)
        interactor.getProducts(offset: offset)
    }
    
    func handleRefreshControl() {
        products = .zero
        offset = 0
        requestProducts()
    }
    
    func didSelect(_ product: ProductModel) {
        guard Request.isReachable else {
            view.didReceive(.connection)
            return
        }
        router.didSelect(product)
    }
    
    func didPressCart() {
        let error = CustomError(with: "Cart action not implemented").error
        view.didReceive(.request(error))
    }
    
    func didPressProfile() {
        let error = CustomError(with: "Profile action not implemented").error
        view.didReceive(.request(error))
    }
}

// MARK: - ProductsInteractorOutput

extension ProductsPresenter: ProductsInteractorOutput {
    func fetchedProducts(_ products: [ProductModel]) {
        loader.dismiss(animated: false)
        view.finishInfiniteScroll()
        if products.isEmpty { return }
        self.products.append(contentsOf: products)
        view.reloadData(with: self.products)
        offset += 1
    }
    
    func didReceive(_ error: ErrorType) {
        if loader.isBeingPresented {
            loader.dismiss(animated: false) { [weak view] in
                view?.didReceive(error)
            }
        } else {
            view.didReceive(error)
        }
    }
}
