import Foundation

protocol ProductsViewOutput {

    func viewIsReady()
    func didPressProfile()
    func didPressCart()
    func didSelect(_ product: ProductModel)
    func requestProducts()
    func handleRefreshControl()
}
