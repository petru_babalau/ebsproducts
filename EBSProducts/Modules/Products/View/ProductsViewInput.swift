import Foundation

protocol ProductsViewInput: class {
    func setupInitialState()
    func reloadData(with items: [ProductModel])
    func didReceive(_ error: ErrorType)
    func showLoader(_ loader: UIAlertController)
    func finishInfiniteScroll()
}
