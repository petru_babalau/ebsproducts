import Foundation
import UIKit

protocol ProductsDataManagerDelegate: class {
    func didSelectItem(_ item: ProductModel)
}

final class ProductsDataManager: NSObject, UITableViewDelegate, UITableViewDataSource {
    var items: [ProductModel]!
    weak var delegate: ProductsDataManagerDelegate?
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return items.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: ProductCell.identifier, for: indexPath) as! ProductCell
        cell.setup(with: items[indexPath.row])
        cell.hideSeparator()
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        delegate?.didSelectItem(items[indexPath.row])
    }
}
