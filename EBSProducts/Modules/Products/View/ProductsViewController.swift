import UIKit
import POSUtils

final class ProductsViewController: UIViewController, StoryboardInstantiable {
	static var storyboardName: String = "ProductsViewController"
    var output: ProductsViewOutput!
    private let dataManager = ProductsDataManager()
    private let refreshControl = UIRefreshControl()
    
    @IBOutlet private weak var tableView: UITableView!

    // MARK: - Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        output.viewIsReady()
    }
}

// MARK: - ProductsViewInput

extension ProductsViewController: ProductsViewInput {
    func setupInitialState() {
        navigationController?.navigationBar.barTintColor = UIColor(r: 27, g: 41, b: 99)
        navigationItem.setLogo()
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.estimatedRowHeight = 337
        setupNavigationItems()
        tableView.tableFooterView = UIView()
        
        registerCell()
        tableView.addInfiniteScroll {[weak self] _ in
            self?.output.requestProducts()
        }
        
        setupRefreshControl()
    }
    
    func reloadData(with items: [ProductModel]) {
        dataManager.items = items
        dataManager.delegate = self
        tableView.delegate = dataManager
        tableView.dataSource = dataManager
        tableView.reloadData()
        refreshControl.endRefreshing()
    }
    
    func didReceive(_ error: ErrorType) {
        let okAction = AlertAction(name: "OK", style: .default)
        let alertInfo = AlertInfo(title: error.message, actions: [okAction])
        let alertController = UIAlertController(info: alertInfo)
        present(alertController, animated: true)
    }
    
    func showLoader(_ loader: UIAlertController) {
        present(loader, animated: false)
    }
    
    func finishInfiniteScroll() {
        tableView.finishInfiniteScroll()
    }
    
    private func setupRefreshControl() {
        tableView.addSubview(refreshControl)
        refreshControl.addTarget(self, action: #selector(handleRefreshControl(_:)), for: .valueChanged)
    }
    
    @objc private func handleRefreshControl(_ sender: UIRefreshControl) {
        output.handleRefreshControl()
    }
    
    private func setupNavigationItems() {
        navigationItem.leftBarButtonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "Profile icon"), style: .plain, target: self, action: #selector(didPressProfile(_:)))
        navigationItem.rightBarButtonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "Cart"), style: .plain, target: self, action: #selector(didPressCart(_:)))
    }
    
    private func registerCell() {
        let nib = UINib(nibName: ProductCell.identifier, bundle: nil)
        tableView.register(nib, forCellReuseIdentifier: ProductCell.identifier)
    }
}

// MARK: - IBActions

extension ProductsViewController {
    @objc private func didPressProfile(_ sender: UIBarButtonItem) {
        output.didPressProfile()
    }
    
    @objc private func didPressCart(_ sender: UIBarButtonItem) {
        output.didPressCart()
    }
}

// MARK: - ProductsDataManagerDelegate

extension ProductsViewController: ProductsDataManagerDelegate {
    func didSelectItem(_ item: ProductModel) {
        output.didSelect(item)
    }
}
