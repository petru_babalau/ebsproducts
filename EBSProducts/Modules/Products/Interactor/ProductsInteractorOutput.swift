import Foundation

protocol ProductsInteractorOutput: class {
    func fetchedProducts(_ products: [ProductModel])
    func didReceive(_ error: ErrorType)
}
