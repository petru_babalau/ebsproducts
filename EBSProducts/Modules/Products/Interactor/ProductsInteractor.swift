import Foundation
import POSUtils

final class ProductsInteractor: ProductsInteractorInput {

    weak var output: ProductsInteractorOutput!

    func getProducts(offset: Int) {
        ProductsFetcher().get(offset: offset, onSuccess: { [weak output] products in
            output?.fetchedProducts(products)
        }) { [weak output] error in
            output?.didReceive(error)
        }
    }
}
