import Foundation

protocol ProductsInteractorInput {
    func getProducts(offset: Int)
}
