import Foundation

protocol ProductsRouterInput {
	var view: ProductsViewInput? {get set}
    func didSelect(_ product: ProductModel)
}
