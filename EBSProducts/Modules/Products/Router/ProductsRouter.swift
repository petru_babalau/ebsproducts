import Foundation

final class ProductsRouter: ProductsRouterInput {
	weak var view: ProductsViewInput?

	private var viewController: ProductsViewController {
        return view as? ProductsViewController !! "can't create view controller"
    }
    
    func didSelect(_ product: ProductModel) {
        let productViewController = ProductViewController.instantiate()
        if let presenter = productViewController.output as? ProductPresenter {
            presenter.didReceive(product)
        }
        viewController.navigationController?.pushViewController(productViewController, animated: true)
    }
}
