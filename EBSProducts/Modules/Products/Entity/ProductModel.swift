import Foundation

struct ProductModel: Decodable {
    let id: Int
    let details: String
    let image: String
    let price: Double
    let sale_precent: Double
    let short_description: String
    let title: String
}
