import UIKit

final class ProductsModuleConfigurator: NSObject {

    @objc func configureModule(for viewController: ProductsViewController) {
        configure(viewController: viewController)
    }

    private func configure(viewController: ProductsViewController) {

        let router = ProductsRouter()

        let presenter = ProductsPresenter()
        presenter.view = viewController
        presenter.router = router

        let interactor = ProductsInteractor()
        interactor.output = presenter

        presenter.interactor = interactor
        viewController.output = presenter
    }
    
}
