import Foundation

protocol ProductModuleInput: class {
    func didReceive(_ product: ProductModel)
}
