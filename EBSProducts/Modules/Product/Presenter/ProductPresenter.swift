import Foundation

final class ProductPresenter {

    weak var view: ProductViewInput!
    var interactor: ProductInteractorInput!
    var router: ProductRouterInput!
    
    private var product: ProductModel!
    private let loader = LoaderAlert(type: .default)
}

// MARK: - ProductModuleInput

extension ProductPresenter: ProductModuleInput {
    func didReceive(_ product: ProductModel) {
        self.product = product
    }
}

// MARK: - ProductViewOutput

extension ProductPresenter: ProductViewOutput {
	func viewIsReady() {
		router.view = view
        view.setuInitialState()
        view.showLoader(loader)
    }
    
    func viewDidAppear() {
        interactor.getProductDetails(product)
    }
    
    func didPressBack() {
        router.didPressBack()
    }
    
    func didPressAddToCart() {
        let error = CustomError(with: "Add to cart action not implemented").error
        view.didReceive(.request(error))
    }
    
    func didPressBuyNow() {
        let error = CustomError(with: "Buy now action not implemented").error
        view.didReceive(.request(error))
    }
}

// MARK: - ProductInteractorOutput

extension ProductPresenter: ProductInteractorOutput {
    func fetchedProduct(_ product: ProductModel) {
        view.reloadData(with: product)
        loader.dismiss(animated: false)
    }
    
    func didReceive(_ error: ErrorType) {
        loader.dismiss(animated: false)
        view.didReceive(error)
    }
}
