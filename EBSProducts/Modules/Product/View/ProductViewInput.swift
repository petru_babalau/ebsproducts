import Foundation

protocol ProductViewInput: class {
    func setuInitialState()
    func didReceive(_ error: ErrorType)
    func reloadData(with product: ProductModel)
    func showLoader(_ loader: UIAlertController)
}
