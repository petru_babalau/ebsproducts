import Foundation
import UIKit

final class ProductDataManager: NSObject, UITableViewDelegate, UITableViewDataSource {
    var product: ProductModel!
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: ProductCell.identifier, for: indexPath) as! ProductCell
            cell.setup(with: product)
            
            return cell
        }
        
        let cell = tableView.dequeueReusableCell(withIdentifier: ProductInfoCell.identifier, for: indexPath) as! ProductInfoCell
        cell.setup(info: product.details)
        
        return cell
    }
}
