import Foundation
import UIKit

class ProductInfoCell: UITableViewCell {
    static let identifier = "ProductInfoCell"
    
    @IBOutlet private weak var infoLabel: UILabel!
    
    func setup(info: String) {
        infoLabel.text = info
    }
}
