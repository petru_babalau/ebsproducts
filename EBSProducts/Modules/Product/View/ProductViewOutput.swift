import Foundation

protocol ProductViewOutput {

    func viewIsReady()
    func viewDidAppear()
    func didPressBack()
    func didPressAddToCart()
    func didPressBuyNow()
}
