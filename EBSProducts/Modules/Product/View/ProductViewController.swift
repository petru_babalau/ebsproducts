import UIKit
import POSUtils

final class ProductViewController: UIViewController, StoryboardInstantiable {
	static var storyboardName: String = "ProductViewController"
    var output: ProductViewOutput!
    private let dataManager = ProductDataManager()
    
    @IBOutlet private weak var tableView: UITableView!

    // MARK: - Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        output.viewIsReady()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        output.viewDidAppear()
    }
}

// MARK: - ProductViewInput

extension ProductViewController: ProductViewInput {
    func setuInitialState() {
        navigationItem.setLogo()
        registerCell()
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.estimatedRowHeight = 337
        setupNavigationItems()
        tableView.tableFooterView = UIView()
    }
    
    func reloadData(with product: ProductModel) {
        dataManager.product = product
        tableView.delegate = dataManager
        tableView.dataSource = dataManager
        tableView.reloadData()
    }
    
    func showLoader(_ loader: UIAlertController) {
        present(loader, animated: false)
    }
    
    func didReceive(_ error: ErrorType) {
        let okAction = AlertAction(name: "OK", style: .default)
        let alertInfo = AlertInfo(title: error.message, actions: [okAction])
        let alertController = UIAlertController(info: alertInfo)
        present(alertController, animated: true)
    }
    
    private func setupNavigationItems() {
        navigationItem.leftBarButtonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "Back Icon"), style: .plain, target: self, action: #selector(didPressBack(_:)))
    }
    
    private func registerCell() {
        let nib = UINib(nibName: ProductCell.identifier, bundle: nil)
        tableView.register(nib, forCellReuseIdentifier: ProductCell.identifier)
    }
}

// MARK: - IBActions

extension ProductViewController {
    @IBAction private func didPressAddToCart(_ sender: UIButton) {
        output.didPressAddToCart()
    }
    
    @IBAction private func didPressBuyNow(_ sender: UIButton) {
        output.didPressBuyNow()
    }
    
    @objc private func didPressBack(_ sender: UIBarButtonItem) {
        output.didPressBack()
    }
}
