import Foundation

protocol ProductRouterInput {
	var view: ProductViewInput? {get set}
    func didPressBack()
}
