import Foundation

final class ProductRouter: ProductRouterInput {
	weak var view: ProductViewInput?

	private var viewController: ProductViewController {
        return view as? ProductViewController !! "can't create view controller"
    }
    
    func didPressBack() {
        viewController.navigationController?.popViewController(animated: true)
    }
}
