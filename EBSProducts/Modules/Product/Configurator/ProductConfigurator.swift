import UIKit

final class ProductModuleConfigurator: NSObject {

    @objc func configureModule(for viewController: ProductViewController) {
        configure(viewController: viewController)
    }

    private func configure(viewController: ProductViewController) {

        let router = ProductRouter()

        let presenter = ProductPresenter()
        presenter.view = viewController
        presenter.router = router

        let interactor = ProductInteractor()
        interactor.output = presenter

        presenter.interactor = interactor
        viewController.output = presenter
    }
    
}
