import Foundation

protocol ProductInteractorInput {
    func getProductDetails(_ product: ProductModel)
}
