import Foundation

protocol ProductInteractorOutput: class {
    func fetchedProduct(_ product: ProductModel)
    func didReceive(_ error: ErrorType)
}
