import Foundation

final class ProductInteractor: ProductInteractorInput {

    weak var output: ProductInteractorOutput!

    func getProductDetails(_ product: ProductModel) {
        ProductsFetcher().get(by: product, onSuccess: { [weak output] response in
            output?.fetchedProduct(response)
        }) { [weak output] error in
            output?.didReceive(error)
        }
    }
}
