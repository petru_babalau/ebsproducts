import XCTest
@testable import EBSProducts

class ProductsPresenterTest: XCTestCase {

    override func setUp() {
        super.setUp()
    }

    override func tearDown() {
        super.tearDown()
    }

    class MockInteractor: ProductsInteractorInput {

    }

    class MockRouter: ProductsRouterInput {

    }

    class MockViewController: ProductsViewInput {

        func setupInitialState() {

        }
    }
}
