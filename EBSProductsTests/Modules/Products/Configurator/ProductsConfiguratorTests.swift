import XCTest
@testable import EBSProducts

class ProductsModuleConfiguratorTests: XCTestCase {

    override func setUp() {
        super.setUp()
    }

    override func tearDown() {
        super.tearDown()
    }

    func testConfigureModuleForViewController() {
        //given
        let viewController = ProductsViewController.instantiate()
        let configurator = ProductsModuleConfigurator()

        //when
        configurator.configureModule(for: viewController)

        //then
        XCTAssertNotNil(viewController.output, "ProductsViewController is nil after configuration")
        XCTAssertTrue(viewController.output is ProductsPresenter, "output is not ProductsPresenter")

        let presenter: ProductsPresenter = viewController.output as! ProductsPresenter
        XCTAssertNotNil(presenter.view, "view in ProductsPresenter is nil after configuration")
        XCTAssertNotNil(presenter.router, "router in ProductsPresenter is nil after configuration")
        XCTAssertTrue(presenter.router is ProductsRouter, "router is not ProductsRouter")

        let interactor: ProductsInteractor = presenter.interactor as! ProductsInteractor
        XCTAssertNotNil(interactor.output, "output in ProductsInteractor is nil after configuration")
    }
}
