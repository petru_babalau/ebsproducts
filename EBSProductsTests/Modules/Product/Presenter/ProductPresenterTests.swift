import XCTest
@testable import EBSProducts

class ProductPresenterTest: XCTestCase {

    override func setUp() {
        super.setUp()
    }

    override func tearDown() {
        super.tearDown()
    }

    class MockInteractor: ProductInteractorInput {

    }

    class MockRouter: ProductRouterInput {

    }

    class MockViewController: ProductViewInput {

        func setupInitialState() {

        }
    }
}
