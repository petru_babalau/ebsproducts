import XCTest
@testable import EBSProducts

class ProductModuleConfiguratorTests: XCTestCase {

    override func setUp() {
        super.setUp()
    }

    override func tearDown() {
        super.tearDown()
    }

    func testConfigureModuleForViewController() {
        //given
        let viewController = ProductViewController.instantiate()
        let configurator = ProductModuleConfigurator()

        //when
        configurator.configureModule(for: viewController)

        //then
        XCTAssertNotNil(viewController.output, "ProductViewController is nil after configuration")
        XCTAssertTrue(viewController.output is ProductPresenter, "output is not ProductPresenter")

        let presenter: ProductPresenter = viewController.output as! ProductPresenter
        XCTAssertNotNil(presenter.view, "view in ProductPresenter is nil after configuration")
        XCTAssertNotNil(presenter.router, "router in ProductPresenter is nil after configuration")
        XCTAssertTrue(presenter.router is ProductRouter, "router is not ProductRouter")

        let interactor: ProductInteractor = presenter.interactor as! ProductInteractor
        XCTAssertNotNil(interactor.output, "output in ProductInteractor is nil after configuration")
    }
}
